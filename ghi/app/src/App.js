import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import VehicleModelList from './VehicleModelList';
import CreateModel from './CreateModel';
import CreateAutomobile from './CreateAutomobile';
import CreateManufacturerForm from './CreateManufacturer';
import CreateSalesperson from './CreateSalesperson';
import AutomobilesList from './AutomobilesList';
import SalespeopleList from './SalespeopleList';
import CustomerList from './CustomerList';
import CreateCustomer from './CreateCustomer';
import SalesList from './SalesList';
import CreateSale from './CreateSale';import TechniciansList from './Technicians';
import CreateTechnicianForm from './CreateTechnician';
import AppointmentsList from './Appointments';
import CreateAppointmentForm from './CreateAppointment';
import SalesHistory from './SalesHistory';
import ServiceHistoryList from './AppointmentsHistory';

function App() {

  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);

  const loadTechnicians = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.status === 200) {
      const data = await response.json();
      setTechnicians(data.technicians);
    };
  };

  const loadAppointments = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.status === 200) {
      const data = await response.json();
      setAppointments(data.appointments);
    };
  };

  useEffect(() => {
    loadTechnicians();
    loadAppointments();
  }, []);


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/">
            <Route index element={<ManufacturersList />} />
            <Route path="create/" element={<CreateManufacturerForm />} />
          </Route>
          <Route path = "models/">
            <Route index element={<VehicleModelList />} />
            <Route path="create/" element={<CreateModel />} />
          </Route>
          <Route path = "automobiles/">
            <Route index element={<AutomobilesList />} />
            <Route path="create/" element={<CreateAutomobile />} />
          </Route>
          <Route path = "salespeople/">
            <Route index element={< SalespeopleList />} />
            <Route path="create/" element={< CreateSalesperson />} />
          </Route>
          <Route path = "customers/">
            <Route index element={< CustomerList />} />
            <Route path="create/" element={< CreateCustomer />} />
          </Route>
          <Route path = "sales/">
            <Route index element={< SalesList />} />
            <Route path="create/" element={< CreateSale />} />
          </Route>
          <Route path="saleshistory/" element={< SalesHistory />} />
          <Route path="technicians/">
            <Route index element={<TechniciansList technicians={technicians} />} />
            <Route path={"create/"} element={<CreateTechnicianForm loadTechnicians={loadTechnicians}/>} />
          </Route>
          <Route path="appointments/">
            <Route index element={<AppointmentsList appointments={appointments} />} />
            <Route path={"create/"} element={<CreateAppointmentForm loadAppointments={loadAppointments} technicians={technicians} />} />
          </Route>
          <Route path="technicians/">
            <Route index element={<TechniciansList technicians={technicians} />} />
            <Route path={"create/"} element={<CreateTechnicianForm loadTechnicians={loadTechnicians}/>} />
          </Route>
          <Route path="appointments/">
            <Route index element={<AppointmentsList appointments={appointments} loadAppointments={loadAppointments} />} />
            <Route path={"create/"} element={<CreateAppointmentForm loadAppointments={loadAppointments} technicians={technicians} />} />
            <Route path={"history/"} element={<ServiceHistoryList appointments={appointments} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
