import React from "react";

function TechniciansList({technicians}) {

  return (
    <div className="px-4 py-5 my-5 text-left">
      <h1 className="display-5 fw-bold">Technicians</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map(technician => {
            return (
              <tr key={technician.id}>
                <td>{technician.employee_id}</td>
                <td className="text-capitalize">{technician.first_name}</td>
                <td className="text-capitalize">{technician.last_name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default TechniciansList;
