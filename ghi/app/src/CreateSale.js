import React, {useEffect, useState} from 'react';

function CreateSale(){
    const [automobiles, setAutomobiles] = useState([]);
    const fetchVinData = async () => {
        const automobilesUrl = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(automobilesUrl);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }
    const [salespeople, setSalespeople] = useState([]);
    const fetchSalespeopleData = async () => {
        const salespeopleUrl =  'http://localhost:8090/api/salespeople/';

        const response = await fetch(salespeopleUrl);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }
    const [customers, setCustomers] = useState([]);
    const fetchCustomerData = async () => {
        const customerUrl = 'http://localhost:8090/api/customers/';

        const response = await fetch(customerUrl);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }
    const [automobile, setAutomobile] = useState('');
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const [salesperson, setSalesperson] = useState('');
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const [price, setPrice] = useState('');
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

    const data = {};
    data.automobile = automobile;
    data.salesperson = salesperson;
    data.customer = customer;
    data.price = price;


    const saleUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(saleUrl, fetchConfig);
    if (response.ok) {

        setAutomobile('');
        setSalesperson('');
        setCustomer('');
        setPrice('');
    }
    }
    useEffect(() => {
        fetchCustomerData();
        fetchSalespeopleData();
        fetchVinData();
    }, []);
    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create A Sale</h1>
                <form onSubmit={handleSubmit} id="create-sale">
                    <div className="form-floating mb-3">
                    <select onChange={handleAutomobileChange} placeholder="Automobile" required type="text" value={automobile} id="Automobile" className="form-select">
                <option value=''>Choose VIN</option>
                {automobiles.map(automobile => {
                    return (
                        <option key={automobile.vin} value={automobile.vin}>
                            {automobile.vin}
                        </option>
                    )
                    })}
                    </select>
                    </div>
                    <div className="form-floating mb-3">
                    <select onChange={handleSalespersonChange} placeholder="Salesperson" required type="text" value={salesperson} id="Salesperson" className="form-select">
                <option value=''>Choose SalesPerson</option>
                {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            {salesperson.first_name} {salesperson.employee_id}
                        </option>
                    )
                    })}
                    </select>
                    </div>
                    <div className="form-floating mb-3">
                    <select onChange={handleCustomerChange} placeholder="Customer" required type="text" value={customer} id="Customer" className="form-select">
                <option value=''>Choose Customer</option>
                {customers.map(customer => {
                    return (
                        <option key={customer.id} value={customer.id}>
                            {customer.first_name} {customer.last_name}
                        </option>
                    )
                    })}
                    </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePriceChange} placeholder="Price" required type="text" value={price} id="Price" className="form-control"/>
                        <label htmlFor="Price">Price</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                    </div>
                    </div>
                    </div>

    )
                };
    export default CreateSale;
