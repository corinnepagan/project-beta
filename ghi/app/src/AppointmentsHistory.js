import React, { useState } from "react";
import { useSearchParams } from "react-router-dom";

function ServiceHistoryList({appointments}) {

  const [searchParams, setSearchParams] = useSearchParams();
  const [stateParams, setStateParams] = useState("");

  const getDateString = (date) => {
    const dateObject = new Date(date);
    const year = dateObject.getFullYear();
    const month = dateObject.getMonth() + 1;
    const day = dateObject.getDate();
    return `${month}/${day}/${year}`;
  };

  const getTimeString = (date) => {
    const dateObject = new Date(date);
    const hours = dateObject.getHours();
    const amOrPm = hours < 12 ? "AM" : "PM";
    const hoursNice = hours <= 12 ? hours : hours - 12;
    const minutes = String(dateObject.getMinutes()).padStart(2, '0');
    const seconds = String(dateObject.getSeconds()).padStart(2, '0');
    return `${hoursNice}:${minutes}:${seconds} ${amOrPm}`;
  };

  return (
    <div className="px-4 py-5 my-5 text-left">
      <h1 className="display-5 fw-bold">Service History</h1>
      <div className="input-group my-3">
        <input type="text" className="form-control" placeholder={searchParams.get("filter") || "Search by VIN..."}
        value={stateParams} onChange={(event) => setStateParams(event.target.value)} />
        <div className="input-group-append">
          {(searchParams.get("filter")) ? <button className="btn btn-secondary" type="button" onClick={() => {
              setSearchParams({});
            }}>X</button> : <button className="btn btn-outline-secondary" type="button" onClick={() => {
          let filter = stateParams;
          if (filter) {
            setSearchParams({ filter });
          } else {
            setSearchParams({});
          }
        }}>Search</button>}
        </div>
      </div>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody className="text-capitalize">
          {appointments.filter((appointment) => {
            let filter = searchParams.get("filter");
            if (!filter) {
              return true;
            }
            let vin = appointment.vin.toLowerCase();
            return vin.startsWith(filter.toLowerCase());
          }).map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.vip.toString()}</td>
                <td>{appointment.customer}</td>
                <td>{getDateString(appointment.date_time)}</td>
                <td>{getTimeString(appointment.date_time)}</td>
                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status === "created" && new Date(appointment.date_time) < new Date() ? "missed" : appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistoryList;
