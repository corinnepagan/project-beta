import React, {useState} from 'react';

function CreateCustomer() {
    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const [address, setAddress] = useState('');
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const [phoneNumber, setPhoneNumber] = useState('');
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;


        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok){



            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
        }
    };
    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
        <h1>Create a Customer</h1>
        <form onSubmit={handleSubmit} id="create-customer-form">
        <div className="form-floating mb-3">
            <input onChange={handleFirstNameChange} placeholder="First name" required type="text" value={firstName} name="firstname" id="firstname" className="form-control"/>
                <label htmlFor="firstname">First Name</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleLastNameChange} placeholder="Last name" required type="text" value={lastName} name="lastname" id="lastname" className="form-control"/>
                <label htmlFor="lastname">Last Name</label>
        </div>
        <div className="form-flaoting mb-3">
            <input onChange={handleAddressChange} placeholder="Address" required type="text" value={address} name="address" id="address" className="form-control"/>
                <label htmlFor="address">Address</label>
        </div>
        <div>
            <input onChange={handlePhoneNumberChange} placeholder="Phone number" required type="text" value={phoneNumber} name="phone number" id="phonenumber" className="form-control"/>
                <label htmlFor="phonenumber">Phone Number</label>
        </div>
        <button className="btn btn-primary">Create</button>
        </form>
        </div>
        </div>
        </div>
    );
}

export default CreateCustomer;
