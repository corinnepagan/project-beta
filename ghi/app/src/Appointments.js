import React from "react";

function AppointmentsList({appointments, loadAppointments}) {

  const getDateString = (date) => {
    const dateObject = new Date(date);
    const year = dateObject.getFullYear();
    const month = dateObject.getMonth() + 1;
    const day = dateObject.getDate();
    return `${month}/${day}/${year}`;
  };

  const getTimeString = (date) => {
    const dateObject = new Date(date);
    const hours = dateObject.getHours();
    const amOrPm = hours < 12 ? "AM" : "PM";
    const hoursNice = hours <= 12 ? hours : hours - 12;
    const minutes = String(dateObject.getMinutes()).padStart(2, '0');
    const seconds = String(dateObject.getSeconds()).padStart(2, '0');
    return `${hoursNice}:${minutes}:${seconds} ${amOrPm}`;
  };

  const handleStatusChange = async (appt, action) => {
    const url = `http://localhost:8080/api/appointments/${appt}/${action}/`;
    const response = await fetch(url, {method: "put",});
    if (response.status === 200) {
      loadAppointments();
    }
  }

  return (
    <div className="px-4 py-5 my-5 text-left">
      <h1 className="display-5 fw-bold">Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th></th>
          </tr>
        </thead>
        <tbody className="text-capitalize">
          {appointments.filter(appointment => {
            return appointment.status === "created" && new Date(appointment.date_time) >= new Date();
          }).map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.vip.toString()}</td>
                <td>{appointment.customer}</td>
                <td>{getDateString(appointment.date_time)}</td>
                <td>{getTimeString(appointment.date_time)}</td>
                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                <td>{appointment.reason}</td>
                <td>
                  <button onClick={() => {handleStatusChange(appointment.id, "cancel")}} type="button" className="btn btn-danger" >Cancel</button>
                  <button onClick={() => {handleStatusChange(appointment.id, "finish")}} type="button" className="btn btn-success" >Finish</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentsList;
