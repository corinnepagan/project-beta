import React, {useEffect, useState} from 'react';
function VehicleModelList(){
    const [vehicleModelList, setvehicleModelList] = useState([])
    async function loadVehicleModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        if(response.ok){
            const data = await response.json();
            setvehicleModelList(data.models);
        }
        else {
            console.error(response);
        }
    }
    useEffect(() => {
        loadVehicleModels();
    }, []);
    return (
        <div>
        <h1 className="fw-bold">Vehicle Models</h1>
        <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {vehicleModelList.map(model => {
                    return (
                        <tr key={model.href}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td>
                                <img src={model.picture_url} alt="" height="100" width="100"></img>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
        </div>
    );
}

export default VehicleModelList;
