import React, { useState } from "react";

function CreateAppointmentForm({loadAppointments, technicians}) {

  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [technician, setTechnician] = useState("");
  const [reason, setReason] = useState("");

  const handleVin = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleCustomer = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleDate = (event) => {
    const value = event.target.value;
    setDate(value);
  }

  const handleTime = (event) => {
    const value = event.target.value;
    setTime(value);
  }

  const handleTechnician = event => {
    const value = event.target.value;
    setTechnician(value);
  }

  const handleReason = event => {
    const value = event.target.value;
    setReason(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.vin = vin;
    data.customer = customer;
    data.date_time = `${date} ${time}`;
    data.technician = technician;
    data.reason = reason;

    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url, fetchOptions);

    if (response.status === 201) {
      loadAppointments();
      setVin("");
      setCustomer("");
      setDate("");
      setTime("");
      setTechnician("");
      setReason("");
    };
  };

  let currentDateAndTime = new Date();
  let todayString = `${currentDateAndTime.getFullYear()}-0${currentDateAndTime.getMonth()+1}-${currentDateAndTime.getDate()}`;
  let yearOutString = `${currentDateAndTime.getFullYear() + 1}-0${currentDateAndTime.getMonth()+1}-${currentDateAndTime.getDate()}`;
  // let nowTimeString = `${currentDateAndTime.getHours()}:${currentDateAndTime.getMinutes()}`;

  return (
    <div className="px-4 py-5 my-5 text-left">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Service Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
              <div className="form-group mb-3">
                <label htmlFor="vin" className="mb-1">Automobile VIN</label>
                <input className="form-control mb-3" type="text" placeholder="VIN..." onChange={handleVin} value={vin} name="vin" id="vin" required />
                <label htmlFor="customer" className="mb-1">Customer</label>
                <input className="form-control mb-3" type="text" placeholder="First and last name..." onChange={handleCustomer} value={customer} name="customer" id="customer" required />

                <label htmlFor="date" className="mb-1">Date</label>
                <input className="form-control mb-3" type="date" min={todayString} max={yearOutString} onChange={handleDate} value={date} name="date" id="date" required />

                <label htmlFor="time" className="mb-1">Time</label>
                <input className="form-control mb-3" type="time" min="08:00" max="19:00" onChange={handleTime} value={time} name="time" id="time" required />

                <label htmlFor="time" className="mb-1">Time</label>
                <select onChange={handleTechnician} name="technician" id="technician" className="form-select mb-3" required>
                  <option value={technician}>Choose a technician...</option>
                  {technicians.map((technician) => {
                    return (
                      <option key={technician.id} value={technician.id}>{technician.employee_id}</option>
                    );
                  })}
                </select>

                <label htmlFor="reason" className="mb-1">Reason</label>
                <input className="form-control mb-3" type="text" placeholder="Reason for appointment..." onChange={handleReason} value={reason} name="reason" id="reason" required />

              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateAppointmentForm;
