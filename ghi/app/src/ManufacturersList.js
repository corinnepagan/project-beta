import React, { useState, useEffect } from 'react';


function ManufacturersList() {

  const [manufacturers, setManufacturers] = useState([]);

  const loadManufacturers = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    loadManufacturers();
  }, []);

  return (
    <div className="px-4 py-5 my-5 text-left">
      <h1 className="display-5 fw-bold">Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.id}>
                <td>{ manufacturer.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturersList;
