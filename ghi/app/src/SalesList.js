import React, {useEffect, useState} from 'react';
function SalesList(){
    const [salesList, setSalesList] = useState([])
    async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok){
            const data = await response.json();
            setSalesList(data.sales)
        }
        else {
            console.error(response);
        }
    }
    useEffect(() => {
        loadSales();
    }, []);
    return (
        <div>
        <h1 className="fw-bold">Sales</h1>
        <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee</th>
                    <th>Employee ID</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {salesList.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
        </div>
    );
}
export default SalesList;
