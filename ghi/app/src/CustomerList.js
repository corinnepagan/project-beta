import React, {useEffect, useState} from 'react';
function CustomerList(){
    const [customersList, setCustomersList] = useState([])
    async function loadCustomers() {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok){
            const data = await response.json();
            setCustomersList(data.customers)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadCustomers();
    }, []);
    return (
        <div>
        <h1 className="fw-bold">Customers</h1>
        <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {customersList.map(customer => {
                    return (
                        <tr key={customer.id}>
                            <td>{customer.first_name} {customer.last_name}</td>
                            <td>{customer.address}</td>
                            <td>{customer.phone_number}</td>
                        </tr>
                    );
                })}
            </tbody>
    </table>
    </div>
    </div>
);
}
export default CustomerList;
