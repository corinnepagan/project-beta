import React, { useState } from "react";
import { NavLink } from "react-router-dom"

function CreateTechnicianForm({loadTechnicians}) {

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeId, setEmployeeId] = useState("");
  const [isSubmitted, setSubmitStatus] = useState(false);

  const handleFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleEmployeeId = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeId;

    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url, fetchOptions);

    if (response.status === 201) {
      loadTechnicians();
      setFirstName("");
      setLastName("");
      setEmployeeId("");
      setSubmitStatus(true);

    };
  };

  return (
    <div className="px-4 py-5 my-5 text-left">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Technician</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input className="form-control" type="text" placeholder="First Name" onChange={handleFirstName} value={firstName} name="first_name" id="first_name" required />
                <label htmlFor="first_name">First name...</label>
              </div>
              <div className="form-floating mb-3">
                <input className="form-control" type="text" placeholder="Last Name" onChange={handleLastName} value={lastName} name="last_name" id="last_name" required />
                <label htmlFor="last_name">Last name...</label>
              </div>
              <div className="form-floating mb-3">
                <input className="form-control" type="text" placeholder="Employee ID" onChange={handleEmployeeId} value={employeeId} name="employee_id" id="employee_id" required />
                <label htmlFor="employee_id">Employee ID...</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      <div className={isSubmitted ? "row" : "d-none"}>
        <div className="offset-3 col-6 text-center">
          <div className="shadow p-4 mt-4">
            <h3>New Technician Successfully Entered!</h3>
            <NavLink to="/"><button type="button" className="btn btn-success m-3">Return Home</button></NavLink>
            <NavLink to="/technicians/"><button type="button" className="btn btn-secondary m-3">Go to Technicians List</button></NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateTechnicianForm;
