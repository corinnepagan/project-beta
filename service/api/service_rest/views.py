from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import TechnicianEncoder, AutomobileVOEncoder, AppointmentEncoder

from .models import Technician, AutomobileVO, Appointment
# Create your views here.


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        response = JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
        response.status_code = 200
        return response
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            response =  JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
            response.status_code = 201
            return response
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_technician(request, pk):
    try:
        technician = Technician.objects.get(id=pk)
        if request.method == "GET":
            response = JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        else:
            count, _ = technician.delete()
            response = JsonResponse(
                {"deleted": count > 0},
            )
        response.status_code = 200
        return response
    except Technician.DoesNotExist:
        response = JsonResponse(
            {"message": "Invalid technician ID number"},
        )
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        response = JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
        response.status_code = 200
        return response
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
            appointment = Appointment.objects.create(**content)
            response = JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
            response.status_code = 201
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Invalid technician ID number"},
            )
            response.status_code = 404
        return response


@require_http_methods(["GET", "DELETE"])
def api_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        if request.method == "GET":
            response = JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        else:
            count, _ = appointment.delete()
            response = JsonResponse(
                {"deleted": count > 0},
            )
        response.status_code = 200
        return response
    except Appointment.DoesNotExist:
        response = JsonResponse(
            {"message": "Invalid appointment ID number"},
        )
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = appointment.CANCELED
        appointment.save()
        response = JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
        response.status_code = 200
        return response
    except Appointment.DoesNotExist:
        response = JsonResponse(
            {"message": "Invalid appointment ID number"},
        )
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = appointment.FINISHED
        appointment.save()
        response = JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
        response.status_code = 200
        return response
    except Appointment.DoesNotExist:
        response = JsonResponse(
            {"message": "Invalid appointment ID number"},
        )
        response.status_code = 404
        return response
