from django.db import models
from django.urls import reverse

# Create your models here.
class Technician(models.Model):
    """
    The Technician model represents a technician entity with its first-name,
    last-name, and employee-id.
    """
    first_name = models.CharField(max_length=55)
    last_name = models.CharField(max_length=55)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.employee_id

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class AutomobileVO(models.Model):
    """
    The AutomobileVO model represents a value object for Automobile object
    instances polled from the Inventory microservice.  It has a VIN property.
    """
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class Appointment(models.Model):
    """
    The Appointment model represents an appointment entity with its date-time,
    reason, status, VIN, customer, and technician fields.
    The status is enforced by model validation choices
    """
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=255)
    CREATED = "created"
    FINISHED = "finished"
    CANCELED = "canceled"
    STATUS_CHOICES = ((CREATED, "created"), (FINISHED, "finished"), (CANCELED, "canceled"),)
    status = models.CharField(
        max_length=20,
        choices=STATUS_CHOICES,
        default=CREATED,
    )
    vin = models.CharField(max_length=17)
    vip = models.BooleanField(default=False)
    customer = models.CharField(max_length=255)
    technician = models.ForeignKey(
        to=Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})

    def __str__(self):
        return f"${self.date_time.isoformat()}-{self.vin}"
