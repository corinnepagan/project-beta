# CarCar

Team:

* Corinne Pagan - Automobile Sales
* Nick Alba - Automobile Service

## How to Run This Project

1.  Fork the repository from https://gitlab.com/nvalba1/project-beta/
2.  Clone the repository to your local machine.
3.  In the project-level folder named project-beta, run the following commands:
    - `docker volume create beta-data`
    - `docker-compose build`
    - `docker-compose up`
4.  In your browser, navigate to `http://localhost:3000` once the react container logs
    show that the webpack has successfully compiled.

![Link to Project Diagram](project-beta-diagram.png)

## Design

#### Service and Sales Pollers - service/poll/poller.py, sales/poll/poller.py,

The `poll` functions for both the Inventory and Sales microservices perform the same functionality with the only differences being which model properties are assigned to their respective value object instances.  The poller.py files import the `AutomobileVO` class from  the relative `.models` path in each microservice's API application directory.

The main function runs every 60 seconds using the `time` module from Python Standard Library.  When run, a get request is made to the host for the Automobiles instance records using the `requests` module from the PSL.  From the JSON-encoded response-content, a list of dictionaries containing Automobile instances is iterated.  Using the Django model method `update_or_create`, `AutomobileVO` object instances are created in each of the microservice's databases.  Existing intances are updated to apply any values that may have changed in already existing `Automobile` instances.  The above logic is performed in the main function body for the Service microservice `poll` function whereas that for the Sales microservice defines it in a helper function `auto` that is called every 60 seconds.  The other key difference between the poller functionality for each of the two microservices is that the `Automobile` class's `sold` and `vin` properties are applied to each `AutomobileVO` instance in Service microservice whereas the Sales microservice only applies the `vin` property to its `AutomobileVO` instances.


## Service microservice

Explain your models and integration with the inventory
microservice, here.

#### Service Models - service/api/service_rest/models.py

- Technician
    - The `Technician` model inherits from the Django model class.  It has the following model properties:
        - `first_name` - <class 'str'> - Character field representing the technician's first name.
        - `last_name` - <class 'str'> - Character field representing the technician's last name.
        - `employee_id` - <class 'str'> - Character field representing the technician's ID typically formatted as '1stInital+LastName'.  Must be unique.
        - `__str__` - <class 'function'> - String dunder method that returns employee_id value.
        - `get_api_url` - <class 'function'> - Assigns 'href' property with URL value for object instance when encoded using ModelEncoder and POST as request method.
- AutomobileVO
    - The `AutomobileVO` model inherits from the Django model class.  It is instantiated by successful requests made by poll/poller.py to the Automobile database records in the Inventory microservice.  It has the following model properties:
        - `vin` - <class 'str'> - Character field representing the automobile value object's VIN.  Must be unique.
        - `sold` - <class 'bool'> - Boolean field indicating whether automobile was sold by dealership.
        - `__str__` - <class 'function'> - String dunder method that returns vin.
- Appointment
    - The `Appointment` model inherits from the Django model class.  It has the following model properties:
        - `date_time` - <class 'datetime.datetime'> - DateTime field representing date and time of service appointment.
        - `reason` - <class 'str'> - Character field representing the reason for the service appointment.
        - `CREATED`, `FINISHED`, `CANCELED` - <class 'str'> - Constant variables assigned to respective choice values for the `status` model property.
        - `STATUS_CHOICES` - <class 'tuple'> - Nested tuple containing pairs of status constants and status string values for the `status` model property.
        - `status` - <class 'str'> - Character field representing the status of the service appointment.  Values enforced by model validation with use of `choices` kwarg assigned to `STATUS_CHOICES` tuple property.  Default value is "created".
        - `vin` - <class  'str'> - Character field representing the VIN of the automobile receiving service.
        - `vip` - <class 'bool'> - Boolean field indicating whether the automobile receiving service was purchased at the dealership.  Assigned by checking for AutomobileVO instance with matching `vin` value when encoded using AppointmentEncoder with POST as request method.
        - `customer` - <class  'str'> - Character field representing the name of the customer requesting the service appointment.
        - `technician` - <class 'service_rest.models.Technician'> - ForeignKey field with many-to-one relationship linking to Technician object instance.  `on_delete` argument set as `PROTECT` to preserve Appointment instances when linked Technician instance is deleted.
        - `get_api_url` - <class 'function'> - Assigns 'href' property with URL value for object instance when encoded using ModelEncoder and POST as request method.
        - `__str__` - <class 'function'> - String dunder method that returns appointment date_time value concatenated with appointment vin value.
***
#### Service URLs - service/api/service_project/urls.py and service/api/service_rest/urls.py

- The Service project directory URLs file registers a top-level API endpoint for the endpoints registered in `service_rest.urls`.  It is mapped to port 8080 and registered using the URL: `http://localhost:8080/api/`  All application endpoints listed below are dependent on that location.
- The Service API application URLs file `api/service_rest/urls.py` registers top-level and dependent resources for the `Technician` and `Appointment` model instances.  They are listed below:

    - `http://localhost:8080/api/technicians/` - Supports GET and POST methods for `Technician` object instances.
    - `http://localhost:8080/api/technicians/:id/` - Supports GET and DELETE methods for specific `Technician` object instances called by their id property as a unique identifier.
    - `http://localhost:8080/api/appointments/` - Supports GET and POST methods for `Appointment` object instances.
    - `http://localhost:8080/api/appointments/:id` - Supports GET, PUT, and DELETE methods for specific `Appointment` object instances called by their id property as a unique identifier.

- The view arguments passed to each `path` call in the `urlpatterns` list are described in the following section.
***
#### Service Views - service/api/service_rest/views.py

_Each of the view functions described below makes use of a custom encoder.  Information on these encoders can be found in the next section_

- `api_technicians` - Supports GET and POST methods.
    - If the request method is GET then a list of all `Technician` instances is assigned to a dictionary with the key `technicians` and encoded as a JSON response using a custom encoder `TechnicianEncoder`.
        <details>
            <summary markdown="span">An example of what a call to <code>api_technicians</code> using a GET request would return is below.</summary>

                <code></code>

                {
                    "technicians": [
                        {
                            "href": "/api/technicians/1/",
                            "id": 1,
                            "first_name": "Bart",
                            "last_name": "Simpson",
                            "employee_id": "bsimpson"
                        },
                        {
                            "href": "/api/technicians/2/",
                            "id": 2,
                            "first_name": "Nelson",
                            "last_name": "Muntz",
                            "employee_id": "nmuntz"
                        },
                        {
                            "href": "/api/technicians/3/",
                            "id": 3,
                            "first_name": "Jimbo",
                            "last_name": "Jones",
                            "employee_id": "jjones"
                        }
                    ]
                }

        </details>


    - If the request method is POST then the JSON formatted body of the request is parsed for all respective model property names listed.  A new `Technician` object is instantiated using the properties and values entered by the user.  The new object instance is encoded as a JSON response using the custom encoder `TechnicianEncoder` with the `safe` argument set to `False` so that non-JSON serializable values returned by the custom encoder can be included in the content body.

        <details>
                <summary markdown="span">An example of of the JSON body passed to <code>api_technicians</code> using a POST request and what the response returns is below.</summary>

                    <code></code>

                    {
                        "first_name": "Jimminy",
                        "last_name": "Cricket",
                        "employee_id": "jcricket"
                    }

        ***


                    {
                        "href": "/api/technicians/4/",
                        "id": 4,
                        "first_name": "Jimminy",
                        "last_name": "Cricket",
                        "employee_id": "jcricket"
                    }

        </details>

- `api_technician` - Supports GET and DELETE methods.
    - If the request method is GET then the object properties of the `Technician` instance specified by the `pk` argument are passed to a single dictionary and encoded as a JSON response using a custom encoder `TechnicianEncoder` with the `safe` argument set to `False`.

        <details>
                <summary markdown="span">An example of what a call to <code>api_technician</code> using a GET request would return is below.</summary>
                    <code></code>

                    {
	                    "href": "/api/technicians/5/",
	                    "id": 5,
	                    "first_name": "Hank",
	                    "last_name": "Hill",
	                    "employee_id": "hhill"
                    }
        </details>

    - If the request method is DELETE then the `Technician` object instance specified by the `pk` argument is removed from the database.  A single dictionary with the key `deleted` and the value a boolean expression returning true if the count returned by the `delete` method is greater than 0 is passed as the JSON response.

        <details>
                <summary markdown="span">An example of what a call to <code>api_technician</code> using a DELETE request would return is below.</summary>
                    <code></code>

                    {
                        "deleted": true
                    }
        </details>

- `api_appointments` - Supports GET and POST methods.
    - If the request method is GET then a list of all `Appointment` instances is assigned to a dictionary with the key `appointments` and encoded as a JSON response using a custom encoder `AppointmentEncoder`.
        <details>
            <summary markdown="span">An example of what a call to <code>api_appointments</code> using a GET request would return is below.</summary>
                <code></code>

                {
                    "appointments": [
                    {
                        "href": "/api/appointments/2/",
                        "id": 2,
                        "date_time": "2023-05-27T15:55:06+00:00",
                        "reason": "Rewire pistons",
                        "status": "created",
                        "vin": "1C3CC5FB2AN120174",
                        "customer": "Jerry Seinfeld",
                        "technician": {
                            "href": "/api/technicians/2/",
                            "id": 2,
                            "first_name": "Kenny",
                            "last_name": "Bania",
                            "employee_id": "kbania"
                        },
                        "vip": true
                    },
                    {
                        "href": "/api/appointments/1/",
                        "id": 1,
                        "date_time": "2023-04-27T14:55:06+00:00",
                        "reason": "Johnson rod",
                        "status": "created",
                        "vin": "1Y9DK4FB2AN120174",
                        "customer": "George Costanza",
                        "technician": {
                            "href": "/api/technicians/1/",
                            "id": 1,
                            "first_name": "Tony",
                            "last_name": "Abado",
                            "employee_id": "tabado"
                        },
                        "vip": false
                    },
                    ]
                }
        </details>


    - If the request method is POST then the JSON formatted body of the request is parsed for all respective model property names listed.  A new `Appointment` object is instantiated using the properties and values entered by the user.  The new object instance is encoded as a JSON response using the custom encoder `AppointmentEncoder` with the `safe` argument set to `False` so that non-JSON serializable values returned by the custom encoder can be included in the request to the service.  The value for the `technician` property is an integer representing the id for an existing `Technician` instance which is retrieved by the view function and assigned to the content body of the request.

        <details>
                <summary markdown="span">An example of of the JSON body passed to <code>api_appointments</code> using a POST request and what the response returns is below.</summary>
                    <code></code>

                    {
                        "date_time": "2022-04-26 22:06",
                        "reason": "Rotate steering wheel",
                        "vin": "9O3VJ7UL2AN185726",
                        "customer": "Bob Sacamano",
                        "technician": 1
                    }

        ***


                    {
                        "href": "/api/appointments/5/",
                        "id": 5,
                        "date_time": "2022-04-26 22:06",
                        "reason": "Rotate steering wheel",
                        "status": "created",
                        "vin": "9O3VJ7UL2AN185726",
                        "customer": "Bob Sacamano",
                        "vip": false
                    }
        </details>

- `api_appointment` - Supports GET and DELETE methods.
    - If the request method is GET then the object properties of the `Appointment` instance specified by the `pk` argument are passed to a single dictionary and encoded as a JSON response using a custom encoder `AppointmentEncoder` with the `safe` argument set to `False`.

        <details>
                <summary markdown="span">An example of what a call to <code>api_appointment</code> using a GET request would return is below.</summary>
                    <code></code>

                    {
                        "href": "/api/appointments/4/",
                        "id": 4,
                        "date_time": "2023-05-03T15:05:06+00:00",
                        "reason": "Change tire fluid",
                        "status": "finished",
                        "vin": "6G9VJ5FB2AN185726",
                        "customer": "Elaine Bennis",
                        "technician": {
                            "href": "/api/technicians/2/",
                            "id": 2,
                            "first_name": "David",
                            "last_name": "Puddy",
                            "employee_id": "dpuddy"
                        },
                        "vip": false
                    }
        </details>

    - If the request method is DELETE then the `Appointment` object instance specified by the `pk` argument is removed from the database.  A single dictionary with the key `deleted` and the value a boolean expression returning true if the count returned by the `delete` method is greater than 0 is passed as the JSON response.

        <details>
                <summary markdown="span">An example of what a call to <code>api_appointment</code> using a DELETE request would return is below.</summary>
                    <code></code>

                    {
                        "deleted": true
                    }
        </details>

- `api_cancel_appointment` - Supports PUT method.
    - The `Appointment` object instance specified by the `pk` argument will have its `status` property changed to 'canceled'.  No content is needed in the request body.

        <details>
                <summary markdown="span">An example of what a call to <code>api_cancel_appointment</code> using a PUT request would return is below.</summary>
                    <code></code>

                    {
                        "href": "/api/appointments/8/",
                        "id": 8,
                        "date_time": "2023-06-12T12:00:00+00:00",
                        "reason": "Steal catalytic converter",
                        "status": "canceled",
                        "vin": "99FHDR8IMK82DJ9OP",
                        "customer": "Cosmo Kramer",
                        "technician": {
                            "href": "/api/technicians/6/",
                            "id": 6,
                            "first_name": "Tim",
                            "last_name": "Whatley",
                            "employee_id": "twhatley"
                        },
                        "vip": false
                    }
        </details>

- `api_finish_appointment` - Supports PUT method.
    - The `Appointment` object instance specified by the `pk` argument will have its `status` property changed to 'finished'.  No content is needed in the request body.

        <details>
                <summary markdown="span">An example of what a call to <code>api_finish_appointment</code> using a PUT request would return is below.</summary>
                    <code></code>

                    {
                        "href": "/api/appointments/9/",
                        "id": 9,
                        "date_time": "2023-05-11T15:00:00+00:00",
                        "reason": "Turpentine belt change",
                        "status": "finished",
                        "vin": "25UEHF8IMK32DJ9OP",
                        "customer": "Susan Ross",
                        "technician": {
                            "href": "/api/technicians/6/",
                            "id": 2,
                            "first_name": "David",
                            "last_name": "Puddy",
                            "employee_id": "dpuddy"
                        },
                        "vip": true
                    }
        </details>

***
#### Service Encoders - service/api/service_rest/encoders.py

- These encoders override the DjangoJSONEncoder to serialize the model data.  Each inherits from `ModelEncoder` which is located in 'service/api/common/json.py'.  The custom API encoders have `model` and `property` properties.  The `model` property is assigned the value of the model class that the encoded object is an instance of, and the `property` property is a list of model properties to be included in each encoded object.  Custom encoders for models which have a `ForeignKey` property will also have an `encoders` property which is a dictionary with the value of the encoder for the related model class.  Encoders with the method `get_extra_data` are provided a custom function that defines the value of the model property when encoded.


## Sales microservice

There are four models within Sales Microservice. These are: `Salesperson`, `Customer`, `Sale`, and `AutomobileVO`. The value objects `Salesperson`, `Customer`, and `AutomobileVO` are utilized in the creation of the `Sale` value object. `AutomobileVO` is a value object of Inventory API's `Automobile model`. This project utilizes a **poll** function to poll 'vin' from the `Inventory API` every 60 seconds, updating or creating an `AutomobileVO` instance.

### Sales Models - **sales/api/sales_rest/models.py**

- Salesperson
    - Model contains:
        - `first_name` - A Character field representing the first name of the salesperson.
        - `last_name` - A Character field representing the last name of the salesperson.
        - `employee_id` - A Character field representing the employee ID of the salesperson.

- Customer
    - Model contains:
        - `first_name` - A Character field representing the first name of the customer.
        - `last_name` - A Character field representing the last name of the customer.
        - `address` - A Character field representing the address of the customer.
        - `phone_number` - a PositiveBigInteger field representing the phone number of the customer.

- AutomobileVO
    - Model contains:
        - `vin` - A unique Character field that polls from the Inventory API's Automobile model.
        - `sold` A Boolean field that is set to false when the automobile is created.

- Sale
    - Model contains:
        - `price` - A PositiveBigInteger field representing the price of the automobile.
        - `automobile` - A ForeignKey field that has a many-to-one relationship with AutomobileVO. Represents the automobile involved in the sale.
        - `salesperson` - A ForeignKey field that has a many-to-one relationship with Salesperson. Represents the salesperson involved in the sale.
        - `customer` - A ForeignKey field that has a many-to-one relationship with Customer. Represents the customer involved in the sale.

This microservice utilizes these models through a view file, that can be found at **sales/api/sales_rest/views.py**. This view file adds functionality to the back-end of this microservice. allowing for future RESTful API calls to occur successfully.

### Sales View Functions - **sales/api/sales_rest/views.py**
These view functions additionally utilize model encoders. These four encoders are:

 - `AutomobileVOEncoder`: A model encoder with the properties of 'vin' and 'sold'. Connected to the AutomobileVO model.
 - `SalespersonListEncoder`: A model encoder with the properties of 'first_name', 'last_name', 'employee_id', and 'id'. 'employee_id' is utilized in the creation of a sale and when looking at a salesperson's sales history. 'id' is used for the "DELETE" request method for the api_delete_salesperson function listed below. Connected to the Salesperson model.
 - `CustomerListEncoder`: A model encoder with the properties of 'id', 'first_name', 'last_name', 'address', 'phone_number'. Connected to the Customer model.
  - `SaleListEncoder`: A model encoder with the properties of 'automobile', 'salesperson', 'customer', 'price', and 'id'. Additionally, `SaleListEncoder` includes `AutomobileVOEncoder` as 'automobile', `CustomerListEncoder` as 'customer', and `SalespersonEncoder` as 'salesperson'.

|Function| Request Methods | Description |
| ---- |----| --- |
| api_salespeople | "GET", "POST" | **GET**: Utilizes `SalespersonListEncoder` to display all model properties. Requests all objects from the Salesperson model to create a list of all salespeople. <br> **POST**: requests body through _json.loads_. Creates a salesperson instance through all model properties listed in `SalespersonListEncoder`.|
| api_delete_salesperson | "DELETE" | **DELETE**: Requests an specific salesperson by their employee ID and deletes the instance. Utilizes `SalespersonListEncoder` to delete all model properties of that specific salesperson.     |
| api_customers | "GET", "POST" | **GET**: Utilizes `CustomerListEncoder` to display all model properties. Requests all objects from the Customer model to create a list of all customers. <br> **POST**: requests body through _json.loads_. Creates a customer instance through all model properties listed in `CustomerListEncoder`.|
| api_delete_customer | "DELETE" | **DELETE**: Requests an specific customer by their ID and deletes the instance. Utilizes `CustomerListEncoder` to delete all model properties of that specific customer.|
| api_sales | "GET", "POST" | **GET**: Utilizes `SaleListEncoder` to display all model properties. Requests all objects from the Sale model to create a list of all sales. <br> **POST**: requests body through _json.loads_. Utilizes _objects.get_ to get the employee_id from the `Salesperson` model, vin from the `AutomobileVO` model, and id from the `Customer` model. Creates a saleinstance through all model properties listed in `SaleListEncoder`. When created, sets the _"sold"_ property of the `AutomobileVO` instance to true.|
| api_delete_sale | "DELETE" | **DELETE**: Requests an specific sale by its ID and deletes the instance. When deleted, sets the _"sold"_ property of the `AutomobileVO` instance to false. Utilizes `SaleListEncoder` to delete all model properties of that specific sale.|


### Sales RESTful API calls - **sales/api/sales_rest/urls.py**
**Salespeople**
|Action| Method | Url |
| ---- |----| --- |
| List all Salespeople | "GET"| http://localhost:8090/api/salespeople/  |
| Create a Salesperson | "POST" | http://localhost:8090/api/salespeople/ |
| Delete a salesperson | "DELETE" | http://localhost:8090/api/salespeople/:id/ |
<details>
<summary markdown="span"> GET: Lists all salespeople with a dictionary that contains all properties listed in SalespersonListEncoder.</summary>
Returns:
<code>

    {
	    "salespeople": [
		    {
			    "first_name": "sally",
			    "last_name": "tyler",
			    "employee_id": "454356",
                "id": 1
		    },
		    {
			    "first_name": "corinne",
			    "last_name": "pagan",
			    "employee_id": "614227",
                "id": 2
		    },
        ]
    }
</code>
</details>

<details>

<summary markdown="span">POST: Requires the first name, last name, and employee ID of the salesperson to successfully create an instance. </summary>
Requires in JSON body request:
<code>

    {
		"first_name": "sally",
		"last_name": "tyler",
		"employee_id": 453453454
    }

</code>

Returns:
<code>

    {
	"first_name": "sally",
	"last_name": "tyler",
	"employee_id": 453453454,
    "id": 1
    }

</code>
</details>
<details>

<summary markdown="span">DELETE: Requires ID of the employee at the end of the URL. </summary>
Initially returns:
<code>

    {
	"first_name": "sally",
	"last_name": "tyler",
	"employee_id": 453453454,
    "id": null
    }

</code>
If sent again:
<code>

    {
	"message": "Salesperson does not exist"
    }

</code>

</details>

**Customers**
|Action| Method | Url |
| ---- |----| --- |
| List all Customers | "GET"| http://localhost:8090/api/customers/  |
| Create a Customer | "POST" | http://localhost:8090/api/customers/ |
| Delete a Customer | "DELETE" | http://localhost:8090/api/customers/:id/ |
<details>
<summary markdown="span"> GET: Lists all customers with a dictionary that contains all properties listed in CustomerListEncoder.</summary>
Returns:
<code>

    {
	"customers": [
		{
			"id": 1,
			"first_name": "sally",
			"last_name": "tyler",
			"address": "456 76th Ave.",
			"phone_number": 3234567891
		},
		{
			"id": 2,
			"first_name": "jonathan",
			"last_name": "adams",
			"address": "555 w 4th ave",
			"phone_number": 313657265
		}
    ]
    }

</code>

</details>
<details>
<summary markdown="span">POST: Requires the first name, last name, address, and phone number of the customer to successfully create an instance. </summary>
Requires in JSON body request:
<code>

    {
			"first_name": "Lola",
			"last_name": "Phillips",
			"phone_number": 3236579831,
			"address": "4465 Beachway Dr."
    }

</code>
Returns:
<code>

    {
	"id": 6,
	"first_name": "Lola",
	"last_name": "Phillips",
	"address": "4465 Beachway Dr.",
	"phone_number": 3236579831
    }

</code>

</details>

<details>

<summary markdown="span">DELETE: Requires ID of the customer at the end of the URL. </summary>
Initially returns:
<code>

    {
	"id": null,
	"first_name": "Lola",
	"last_name": "Phillips",
	"address": "4465 Beachway Dr.",
	"phone_number": 3236579831
    }

</code>
If sent again:
<code>

    {
	"message": "Customer does not exist"
    }

</code>
</details>

**Sales**
|Action| Method | Url |
| ---- |----| --- |
| List all Sales | "GET"| http://localhost:8090/api/sales/  |
| Create a Sale | "POST" | http://localhost:8090/api/sales/ |
| Delete a Sale | "DELETE" | http://localhost:8090/api/sales/:id/ |
<details>
<summary markdown="span"> GET: Lists all sales with a dictionary that contains all properties listed in SaleListEncoder.</summary>
Returns:
<code>

    {
        "sales": [
            {
                "automobile": {
                    "vin": "456765766",
                    "sold": true
                },
                "salesperson": {
                    "first_name": "allison",
                    "last_name": "moovile",
                    "employee_id": "343243",
                    "id": 3
                },
                "customer": {
                    "id": 6,
                    "first_name": "Lola",
                    "last_name": "Phillips",
                    "address": "4465 Beachway Dr.",
                    "phone_number": 3236579831
                    },
                "price": 343243,
                "id": 4
            }
        ]
    }

</code>
</details>
<details>
<summary markdown="span">POST: Requires the vehicle vin, price, employee ID, and customer ID to successfully create an instance. Cannot create two sales with the same vin. </summary>
Requires in JSON body request:
<code>

    {
	"price": 15000,
	"automobile": "900765",
	"salesperson": "343243",
	"customer": 2
    }

</code>
Returns:
<code>

    {
	"automobile": {
		"vin": "900765",
		"sold": true
	},
	"salesperson": {
		"first_name": "allison",
		"last_name": "moovile",
		"employee_id": "343243",
		"id": 3
	},
	"customer": {
		"id": 2,
		"first_name": "jonathan",
		"last_name": "adams",
		"address": "555 w 4th ave",
		"phone_number": 313657265
	},
	"price": 15000,
	"id": 5
    }

</code>
If two sales with the same vin are attempted:

<code>

    {
        "message": "cannot create sale"
    }

</code>

</details>

<details>
<summary markdown="span">DELETE: Requires id of the sale at the end of the URL. </summary>
Initially returns:
<code>

    {
	"automobile": {
		"vin": "456765766",
		"sold": false
	},
	"salesperson": {
		"first_name": "allison",
		"last_name": "moovile",
		"employee_id": "343243",
		"id": 3
	},
	"customer": {
		"id": 3,
		"first_name": "griffin",
		"last_name": "lanes",
		"address": "555 w 4th ave",
		"phone_number": 3107765847
	},
	"price": 343243,
	"id": null
    }

</code>
If sent again:
<code>

    {
	"message": "Sale does not exist"
    }

</code>

</details>
