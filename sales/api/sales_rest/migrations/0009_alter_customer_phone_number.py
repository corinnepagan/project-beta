# Generated by Django 4.0.3 on 2023-04-28 16:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0008_automobilevo_sold'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='phone_number',
            field=models.PositiveBigIntegerField(),
        ),
    ]
